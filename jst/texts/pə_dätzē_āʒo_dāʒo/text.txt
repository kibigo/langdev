Ʒā pœt zafo ʒā dätzē dotl’go.
Pǝ dāso zafo hämātl’go pǝ dätzē.
Ʒā ħœfo dohil’go su.
Ʒā tuād ʒoħ jäsātol’gi tusol’go su.
Sat äħꜵ pǝ dāso tutā pǝ dätzē, mœ pǝ hoʒā ʒoħ dosil’go su.

“Pə dätzē mœ pə fäʒēl”

Pə dätzē āʒo hoʒā ʒoħ ʒoħʒəl’ħə iə ʒā fäʒēl vodə howosēl’go su.
Jäsātol’go su, “Odo!”, o su ʒoħ fil’jäsātol’go pə fäʒēl.
Jäsātol’go su, “Jäsātol’gi howāl əwādl?”.
Su ʒoħ fil’jäsātol’go sul.
Zo ʒā so fil’dotl’go howāl’ħə howosēl’go pə dätzē; sat äħꜵ totāl’go su.
