# The LANGDEV Repository

This repository forms a monorepo of linguistic information and data
pertaining to languages developed by [the LANGDEV Project][LANGDEV]. An
overview of these languages follows :—

## Languages of Current Interest

### The Jastu‐Sevensi Languages

**Language code:** `art-x-jsv` (use a more specific code if possible)

#### [Pre–Jastu‐Sevensi](./jsv/0009/)

**Language code:** `art-x-jsv-0009`

Pre–Jastu‐Sevensi forms the foundation for all of the Jastu‐Sevensi
languages. It was developed starting in 2015 by refining Jastugay and
incorporating aspects of the other 7c languages (see below).

New developments in Pre–Jastu‐Sevensi should be undertaken only
reluctantly and with much care. Deriving new words in descendant
languages from the existing lexis is much preferred where possible.

<!--
#### [Classical Sevensi](./osv/)

**Language code:** `art-x-osv`
-->

## Historical Language Efforts

These languages are considered “noncanonical”, meaning that they are
not taken to comprise part of the fictional history of LANGDEV
languages and should not be attested in new works.

### The “7c” Languages

These languages were developed by [Margaret KIBI][kibigo!] while she
was in high school. Work on these languages had probably begun by 2009
and definitely by 2010, and ceased sometime in 2012 in favour of the
successor Jastugay language (described below). Later, these languages
were incorporated into the starting lexicon for Pre–Jastu‐Sevensi.

The 7c languages share a common feature of being developed iteratively
from an extremely small starting base. Each iteration was assigned a
number; for Eho the numbers range from Ⅰ–ⅠⅤ, for Elrex from Ι–ⅤⅠ, and
for Jastulae from Ι–ⅤⅠⅠⅠ. The LANGDEV Project makes no claims as to
whether this was a good or practical way to develop a language.

#### [Eho](./eho/)

**Language code:** `art-x-eho`

The Eho language had the smallest starting lexicon of the 7c languages,
with just two words. Only four iterations were completed, at which time
the final lexis was still only 15 words.

The name “Eho” is derived from “East Hope”, an English exonym for the
fictional location from which the language was thought to originate.
The name was only used in supplemental materials; in actual language
documentation the symbol <i lang="zxx">□</i> was used instead.

#### [Elrex](./lrx/)

**Language code:** `art-x-lrx`

The Elrex language was highly onomatopœic, with a large number of
vowels and a lexicon which tended to deal in feelings or emotions. Five
iterations were completed; the sixth was started but never finished.

The name “Elrex” derives from the pronunciation of “Lrex”, itself an
abbreviated form of “Lendrex” ⟨< PIE <i lang="ine-x-proto">\*lendʰ</i>
“land” + <i lang="ine-x-proto">\*h₃rḗǵs</i> “king”⟩, an exonym for the
fictional location from which the language was thought to originate.

#### [Jastulae](./jtl/)

**Language code:** `art-x-jtl`

Jastulae is the most advanced of the 7c languages, developed through
eight full iterations and, in its final form, consisting of a
substantial lexis and rudimentary grammar. It is the only 7c language
to have an endonym; the word <i lang="art-x-jtl-0008">jāsto͡olæ</i> in
Jastulae ⅤⅠⅠⅠ means “speaking people”. Prior to the development of this
word, the language was known vernacularly by the name <i>Trustian</i>,
and in documentation by the symbol <i lang="zxx">⊻</i>.

### [Jastugay](./jst/)

**Language code:** `art-x-jst`; historically also `x-XX-jt`

Jastugay, natively written <i>Jästugā</i>, was a refinement and
expansion of Jastulae ⅤⅠⅠⅠ (described above). The boundary between the
two is somewhat fuzzy; Jastugay is distinguished by having a more
refined grammatical system even as it shares much of the same
vocabulary. Work on this language primarily occurred in 2012 and 2013.

Only one text in Jastugay was ever produced: a short snippet of story
titled <cite lang="art-x-jst">Pə dätzē āʒo dāʒo</cite>. It is located,
alongside a gloss, in `jst/texts/pə_dätzē_āʒo_dāʒo/`. This text notably
does not line up precisely with the language as documented and likely
predates several innovations, placing it somewhere on the boundary
between the Jastulae and Jastugay dialects.

[LANGDEV]: https://pro.ject-lang.dev/
[kibigo!]: https://go.KIBI.family/About/#me
