/* jslint asi:true, browser:true */
/* globals Lexis */

function init() {
    Lexis.Viewer.load("index.xml");
}

window.addEventListener("load", init, false);
